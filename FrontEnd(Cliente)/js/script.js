


//Variables necesarias que se usan a lo largo de la ejecucion
var nombreUsuario, player2,jugadores,miConexion,maximo=0,playerName="",names="",jugadorActual,posicionesArreglo,contadorSorts=1;
var tableroalInicio,fichasturno=0, sumadorInicial=0,contadorSortsInicio=0;
cont=1,cont2=0 ;

var id,ip;
var websocket;
var wait=ms=> new Promise((r,j)=>setTimeout(r,ms)); 


//En este document.ready se pedira el ID de la sala, si el Id no es valido, se pide otro 
$('document').ready(function(){
   
    $('#botonIniciar').hide();
    $('#robar').hide();
    // $('#fichasS').hide();
    // $('#imgPilaFichas').hide();
    $('#botonListo').hide();
    Swal.fire({
        title: "Ingrese el ID de la sala",
        input: 'text',
        showCancelButton: true, 
        inputValidator: nombre => {
            // Si el valor es válido, debes regresar undefined. Si no, una cadena
            if (!nombre) {
                return "Por favor, escribe el ID de la sala";
            } else {
                return undefined;
            }
        }       
    })
    .then((result) => {
        if (result.value) {
          
            id=result.value;
            cont2++;
            idBien();
        }
    });
    
});

////////////////////////////////////////////////////
//Final mensaje de ID de la sala


     
     
//En esta funcion, se valida el id de la sala para poder conectar al usuario con el servidor,
//Aqui estan todos los metodos de comunicacion con el servidor
     function idBien(){
        if(cont2==1){

            //notar el protocolo.. es 'ws' y no 'http'
            var wsUri = "ws://"+id+":30001";
            websocket = new WebSocket(wsUri);
               //creamos el socket


            //Websocket OnOpen
               websocket.onopen = function(evt) { //manejamos los eventos...
                console.log("Conectado..."); //... y aparecerá en la pantalla
                ping();
            
            };


            //Websocket OnMessage
            websocket.onmessage = function(evt) { // cuando se recibe un mensaje
                evt = JSON.parse(evt.data);
                if(evt.type=="empate"){
                    $(".audioEmpate")[0].play();
                    Swal.fire({
                        title: 'El juego esta empatado',
                        text: 'Ya no hay mas fichas que robar',
                        imageUrl: 'img/empate.png',
                        imageWidth: 500,
                        imageHeight: 300,
                        imageAlt: 'Custom image',
                    }).then((result) => {
                        nombreUsuario="";
                        player2="";
                        jugadores=0;
                        miConexion=0;
                        maximo=0;
                        playerName="";
                        names="";
                        jugadorActual="";
                        posicionesArreglo=0;
                        contadorSorts=1;
                        tableroalInicio="";
                        fichasturno=0;
                        sumadorInicial=0;
                        contadorSortsInicio=0;
                        cont=1;
                        cont2=0 ;
                        window.location.replace('index.html');
                    });
                }
                if(evt.type=="pilaFichasR"){
                    fichaRobada=JSON.parse(evt.message);
                    if (evt.tipoJugada=="robarFichas") {
                        pintarRobada(fichaRobada);
                        pasarturno("bien",-1);
                    }else if (evt.tipoJugada=="jugadaerronea") {
                        pintarRobada(fichaRobada);
                    }
                }
                if(evt.type=="mireparticion"){
                    misfichas=JSON.parse(evt.message);
                    pintarReparticion(misfichas);
                }
                if(evt.type=="sorteoTurnos"){

                    ficha=JSON.parse(evt.message);
                    pintarSorteo(ficha);

                }
                if(evt.type=="Overflow"){
                    
                    Swal.fire({
                        icon: 'error',
                        title: 'Sala de juego llena',
                        showConfirmButton: false,
                        timer: 2000
                      })
                      .then((result) => {
                        window.location.replace('index.html');
                      });
               }else if(evt.type=="NoOverflow"){
                    peticionNombreUsuario();
               }
               if(evt.type=="Validacionusername"){
                   if(evt.message=="nombre valido"){
                        Swal.fire({
                            icon: 'success',
                            title: 'Nombre de usuario aceptado',
                            showConfirmButton: false,
                            timer: 2000
                        });
                        nombreUsuario=evt.username;
                        data = {
                            type:'username',
                            username:nombreUsuario
                        };
                        var mes=JSON.stringify(data);
                        websocket.send(mes);
                   }else if(evt.message=="nombre no valido"){
                        Swal.fire({
                            icon: 'error',
                            title: 'Nombre de Usuario No Valido',
                            text: 'Ya existe un jugador con ese nombre de usuario.',
                            showConfirmButton: false,
                            timer: 2000
                        }).then((result) => {
                            peticionNombreUsuario();
                        });
                   }
               }
                if(evt.type=="Conexionexterno"){
                    cont++;
                    jugadores++;
                     pintarmensaje(cont,evt.username);
                     names[jugadores-1]=evt.username;
                }
                if(evt.type=="Conexionlocal"){
                    jugadores = evt.conectados;
                    miConexion=jugadores;
                    if(jugadores==1){

                        $('#botonIniciar').show(100);
                    }
                        var nombres = evt.Users;
                        nombres = nombres.substring(1,nombres.length-1);
                        names = nombres.split(', ');
                        for(var y=1;y<=jugadores;y++){
                            if(y==miConexion){
                                 pintarmensaje(y,nombreUsuario);
                                
                            }else{
                                var user='jugador'+y;
                                pintarmensaje(y,names[y-1]);
                                cont++;
                            }
                        } 
                }

                if (evt.type=="jugadorActual") {
                    // if (sumadorInicial==0) {
                    //     for (let index = 1; index < contadorSortsInicio; index++) {
                    //         $("#sortablea"+index).sortable("disable");
                    //     }
                    // }else{
                    //         $(".sortable").sortable("enable");
                    // }
                    
                    if (evt.tipo=="bien") {
                        var num=names.indexOf(evt.message);
                        if(num==0){
                          jugadorActual=names[names.length-1];
                        }else{
                            jugadorActual=names[num-1];
                        }
                        if(evt.fichitas==0){
                            $(".audioGanador")[0].play();
                            Swal.fire({
                                title: 'El juego ha terminado!!!!',
                                text: jugadorActual+' ha ganado el chico',
                                imageUrl: 'img/ganador.jpg',
                                imageWidth: 500,
                                imageHeight: 300,
                                imageAlt: 'Custom image',
                            }).then((result) => {
                                nombreUsuario="";
                                player2="";
                                jugadores=0;
                                miConexion=0;
                                maximo=0;
                                playerName="";
                                names="";
                                jugadorActual="";
                                posicionesArreglo=0;
                                contadorSorts=1;
                                tableroalInicio="";
                                fichasturno=0;
                                sumadorInicial=0;
                                contadorSortsInicio=0;
                                cont=1;
                                cont2=0 ;
                                window.location.replace('index.html');
                            });
                        }
                        
                        if(evt.fichitas!=-1){
                            actualizarFichasExternas(evt.fichitas,jugadorActual);
                        }
                        tableroalInicio= document.getElementById("body").innerHTML;
                        contadorSortsInicio=contadorSorts;
                        console.log('el contador de sorts al inicio es= '+contadorSortsInicio);
                    }
                   
                   
                    jugadorActual = evt.message;
                    posicionJugador = names.indexOf(jugadorActual);
                    resaltarActual(jugadorActual);
                    gestionTurnos(jugadorActual);
                    // estadoAnterior = document.getElementById("body").innerHTML;
                   
                    
                }

//////////////////////////////////////////////////////////////////////////////////
//Este mensaje es el que se encarga de actualizar los movimientos para todos los jugadores

                if(evt.type=="updateSortable"){
                    $("#sortableGrande").empty();
                    $("#sortableGrande").prepend(evt.sortableGrande);
                    fichasDragg();
                    sort(); 
                }
                if(evt.type=="contadorSort"){
                    contadorSorts=evt.cant;
                    console.log("los sorts son desde onmmess - "+contadorSorts); 
                }
    
                if (evt.type=="RoboJugadorExterno") {
                    var indexusuariorobo = names.indexOf(evt.username)+1;
                    pintarRoboExterno(indexusuariorobo,jugadores);
                }
                
                if(evt.message=="pong"){
                    ping();
                }

                // ESTO ES PARA ACTUALIZAR A ESTADO ANTERIOR EN JUGADA ERRONEAS DE OPTROS JUGADORES
                if(evt.type=="jugadaErrada"){
                    $("#body").empty();
                    $("#body").prepend(tableroalInicio);
                    contadorSorts=contadorSortsInicio
                    fichasDragg();
                    sort();
                }
            };

            //Websocket OnError
            websocket.onerror = function(evt) {
                 Swal.fire({
                    icon: 'error',
                    title: 'ID de la sala invalido',
                    showConfirmButton: false,
                    timer: 2000
                  })
                  .then((result) => {
                    window.location.replace('index.html');
                  });
                 
            };
         }
     }
     
///////////////////////////////////////////////////////

// En esta funcion se valida el nombre de usuario
function peticionNombreUsuario(){
    Swal.fire({
        title: "Nombre de usuario",
        text: "Ingrese su nombre de Usuario:",
        input: 'text',
        showCancelButton: true, 
        inputValidator: nombre => {
            // Si el valor es válido, debes regresar undefined. Si no, una cadena
            if (!nombre) {
                return "Por favor, escribe tu nombre de usuario";
            } else {
                return undefined;
            }
        }       
    })
    .then((result) => {
        data = {
            type:'Verifyusername',
            username:result.value
        };
        var mes=JSON.stringify(data);
        websocket.send(mes);
    });
}
   
//En esta seccion se gestiona el inicio de partida
$('#botonIniciar').click(function(){
    if(jugadores>=2){
        $('#botonIniciar').hide(100);
        data={
            type:"inicioPartida",
            message:"numeroMayor"
        };
        dataToSend=JSON.stringify(data);
        websocket.send(dataToSend);
    }else{
        Swal.fire({
            icon: 'error',
            title: 'Faltan jugadores para iniciar la partida',
            showConfirmButton: false,
            timer: 2000
          })
    };


});
      
       
//En esta funcion se pintan las tarjetas correspondientes a cada jugador
function pintarSorteo(fichas){
    //Cuando hay 2 jugadores
    if(jugadores==2){
        if(miConexion==1){
            posicionAbajo=0;
            posicionArriba=1;
        }else{
            posicionAbajo=1;
            posicionArriba=0;
        }

     posicion=pintarNumeroMayor(fichas,posicionAbajo,"Abajo");
    
    $("#Abajo").append(posicion);

    posicion=pintarNumeroMayor(fichas,posicionArriba,"Arriba");
        $("#Arriba").append(posicion);
        animacionSorteoFichasL();
    }

    //Cuando hay 3 jugadores
    if(jugadores==3){

        if(miConexion==1){
            posicionAbajo=0;
            posicionIzquierda=1;
            posicionArriba=2;
           }else if(miConexion==2){
            posicionAbajo=1;
            posicionIzquierda=2;
            posicionArriba=0;
           }else{
            posicionAbajo=2;
            posicionIzquierda=0;
            posicionArriba=1;
           }

        posicion=pintarNumeroMayor(fichas,posicionAbajo,"Abajo");
          $("#Abajo").append(posicion);

  
        posicion=pintarNumeroMayor(fichas,posicionArriba,"Arriba");
        $("#Arriba").append(posicion);

 
        posicion=pintarNumeroMayor(fichas,posicionIzquierda,"Izquierda");
        $("#Izquierda").append(posicion);
        animacionSorteoFichasL();
    }

    //Cuando hay 4 jugadores
    if(jugadores==4){
        if(miConexion==1){
            posicionAbajo=0;
            posicionIzquierda=1;
            posicionArriba=2;
            posicionDerecha=3;
           }else if(miConexion==2){
            posicionAbajo=1;
            posicionIzquierda=2;
            posicionArriba=3;
            posicionDerecha=0;
           }else if(miConexion==3){
            posicionAbajo=2;
            posicionIzquierda=3;
            posicionArriba=0;
            posicionDerecha=1;
           }else{
            posicionAbajo=3;
            posicionIzquierda=0;
            posicionArriba=1;
            posicionDerecha=2;
           }

           posicion=pintarNumeroMayor(fichas,posicionAbajo,"Abajo");
          $("#Abajo").append(posicion);
  
        posicion=pintarNumeroMayor(fichas,posicionArriba,"Arriba");
        $("#Arriba").append(posicion);
 
        posicion=pintarNumeroMayor(fichas,posicionIzquierda,"Izquierda");
        $("#Izquierda").append(posicion);

        posicion=pintarNumeroMayor(fichas,posicionDerecha,"Derecha");
        $("#Derecha").append(posicion);

        animacionSorteoFichasL();

    }
   

    Swal.fire({
        imageUrl: 'img/iconStart.png',
        imageWidth: 100,
        imageHeight: 100,
        imageAlt: 'Custom image',
        title: 'El numero mayor es: '+maximo,
        html:'<p class="textAlertWin">El primer turno es para el jugador: '+'<b>'+names[playerName]+'</b></p>',
        showConfirmButton: false,
        timer: 5000,
      }).then((result) => {
        
        repartirfichas();
        $(".audio")[0].play();
        
        posicionJugador = names.indexOf(names[playerName]);
       
    });   
}


function botonListoFuncion() {
    var verificacionT = ValidarJugada();

    if (verificacionT == "true") {
        sumadorInicial++;
        if (sumadorInicial == 1) {
            var numero = 0;
            var sorticos = contadorSorts - 1
            for (let index = sorticos; index > contadorSortsInicio - 1; index--) {
                var Arreglo = [];
                var contenido = document.getElementById("sortablea" + index).innerHTML;
               
                var splitFichas = contenido.split('</div>');
                if (splitFichas.length == 1) {
                } else {
                    for (let i = 0; i < splitFichas.length - 1; i++) {
                        var splitParametros = splitFichas[i].split('R');
                        data = {
                            color: splitParametros[1],
                            numero: splitParametros[2],
                            tipo: splitParametros[3]
                        }
                        Arreglo[i] = JSON.stringify(data);
                    }
                    var array = JSON.parse(Arreglo[0]);
                    var array2 = JSON.parse(Arreglo[1]);
                    var array3 = JSON.parse(Arreglo[2]);
                    var esSerie = "no";
                    var esEscalera = "no";
                    if (array.numero == array2.numero || array.numero == array3.numero || array2.numero == array3.numero) {
                        esSerie = "si";
                    } else {
                        esEscalera = "si";
                    }
                    for (let j = 0; j < Arreglo.length; j++) {
                        var arraysuma = JSON.parse(Arreglo[j]);
                        if (arraysuma.tipo == "comodin") {
                            if (esSerie = "si") {
                                if (j == Arreglo.length - 1) {
                                    var arraysuma2 = JSON.parse(Arreglo[j - 1]);
                                    arraysuma.numero = arraysuma2.numero;
                                } else {
                                    var arraysuma2 = JSON.parse(Arreglo[j + 1]);
                                    arraysuma.numero = arraysuma2.numero;
                                }
                            } else if (esEscalera == "si") {
                                if (j == Arreglo.length - 1) {
                                    var arraysuma2 = JSON.parse(Arreglo[j - 1]);
                                    arraysuma.numero = arraysuma2.numero + 1;
                                } else {
                                    var arraysuma2 = JSON.parse(Arreglo[j + 1]);
                                    arraysuma.numero = arraysuma2.numero - 1;
                                }
                            }
                        }
                        numero = numero + parseInt(arraysuma.numero);
                    }
                }

            }
            if (numero < 30) {
                Malajugada();
                sumadorInicial = 0;
            } else {
                var fichasFinalTurno = document.getElementById("misfichasAbajo").innerHTML;
                fichasFinalTurno = fichasFinalTurno.split('</div>');
                fichasturno = fichasFinalTurno.length - 1;
                pasarturno("bien", fichasturno);
            }
        } else {
            
            var fichasFinalTurno = document.getElementById("misfichasAbajo").innerHTML;
            fichasFinalTurno = fichasFinalTurno.split('</div>');
            fichasturno = fichasFinalTurno.length - 1;
            pasarturno("bien", fichasturno);
        }

    } else {
        Malajugada();
    }

}

function Malajugada(){
   
        $("#body").empty();
        $("#body").prepend(tableroalInicio);
        sort();
        roboFicha("jugadaerronea");
        roboFicha("jugadaerronea");
        roboFicha("jugadaerronea");
        Swal.fire({
            icon: 'error',
            title: 'Mala jugada, se te agregan 3 fichas',
            showConfirmButton: false,
            timer: 4000
        }).then((result) => {
            pasarturno("mal");
        });
        data = {
            type: "jugadaErrada"
        };
        contadorSorts=contadorSortsInicio;
        var fichasFinalTurno=document.getElementById("misfichasAbajo").innerHTML;
        fichasFinalTurno=fichasFinalTurno.split('</div>');
        fichasturno=fichasFinalTurno.length+2;
        var dataToSend=JSON.stringify(data);
        websocket.send(dataToSend);
}

function pasarturno(tipopasar,fichitas){

    $("#botonListo").hide();
    $("#minombre").removeClass("jugadoractual");
    posicionJugador++;
    if (posicionJugador <= posicionesArreglo) {
        jugadorActual = names[posicionJugador];
    }else{
        posicionJugador=0;
        jugadorActual = names[posicionJugador];
    }
    data={
        type:"jugadorActual",
        message: jugadorActual,
        tipo: tipopasar,
        fichitas:fichitas
    };

    resaltarActual(jugadorActual);
    $( ".ui.card.fichaL.misfichas" ).draggable("disable");
    $( ".sortable").sortable("disable");
    dataToSend=JSON.stringify(data);
    websocket.send(dataToSend);
  
}

function resaltarActual(nombreActual){
    
    if(jugadores>=2){
        if(document.getElementById("nArriba").innerHTML==nombreActual){
            $("#nArriba").addClass("jugadoractual");
            $("#nDerecha").removeClass("jugadoractual");
            $("#nIzquierda").removeClass("jugadoractual");
        }else{
            $("#nArriba").removeClass("jugadoractual");
        }
    }
    if(jugadores>=3){
        if(document.getElementById("nIzquierda").innerHTML==nombreActual){
            $("#nIzquierda").addClass("jugadoractual");
            $("#nDerecha").removeClass("jugadoractual");
            $("#nArriba").removeClass("jugadoractual");
        }else{
            $("#nIzquierda").removeClass("jugadoractual");
        }
    }
    if(jugadores>=4){
        if(document.getElementById("nDerecha").innerHTML==nombreActual){
            $("#nDerecha").addClass("jugadoractual");
            $("#nArriba").removeClass("jugadoractual");
            $("#nIzquierda").removeClass("jugadoractual");
        }else{
            $("#nDerecha").removeClass("jugadoractual");
        }
    } 
}

function gestionTurnos(nombreGanador) {
  
    // jugadorActual = nombreGanador;
    jugadorActual=nombreGanador;
    if (nombreUsuario == nombreGanador) {
        $("#botonListo").show();
        $( ".ui.card.fichaL.misfichas" ).draggable("enable");
        $( ".sortable" ).sortable("enable");
        $("#minombre").addClass("jugadoractual");
        resaltarActual(nombreGanador);
    }else{
        resaltarActual(nombreGanador);
        $("#botonListo").hide();
        $( ".ui.card.fichaL.misfichas" ).draggable("disable");
        // $( ".sortable").sortable("disable");
    }
}

function esconderFichasL() {
    $(".ui.card.fichaL.Arriba").hide();
    $(".ui.card.fichaL.Arriba").transition('horizontal flip');
    $(".ui.card.fichaL.Abajo").hide();
    $(".ui.card.fichaL.Abajo").transition('horizontal flip');
    $(".ui.card.fichaL.Derecha").hide();
    $(".ui.card.fichaL.Derecha").transition('horizontal flip');
    $(".ui.card.fichaL.Izquierda").hide();
    $(".ui.card.fichaL.Izquierda").transition('horizontal flip');
}

function animacionSorteoFichasL() {
    $(".ui.card.fichaL.Arriba").hide();
    $(".ui.card.fichaL.Arriba").transition('horizontal flip',2000);
    $(".ui.card.fichaL.Abajo").hide();
    $(".ui.card.fichaL.Abajo").transition('horizontal flip',2000);
    $(".ui.card.fichaL.Derecha").hide();
    $(".ui.card.fichaL.Derecha").transition('horizontal flip',2000);
    $(".ui.card.fichaL.Izquierda").hide();
    $(".ui.card.fichaL.Izquierda").transition('horizontal flip',2000);
}

function pintarNumeroMayor(fichas,valor,posicion){
    if((fichas[valor].numero)>maximo){
        playerName=valor;
        maximo=fichas[valor].numero;
    }
    
    datos='<div class="ui container">';
            datos+='<div class="ui centered cards">';
                       datos+=' <div class="ui card fichaL '+posicion+'">';
                            datos+='<h2 id="fichasorteo1"class="numeroFicha color'+fichas[valor].color+'">'+fichas[valor].numero+'</h2>';
                        datos+='</div>';
            datos+=' </div>';
        datos+='</div>';
        
        return datos;
}

function repartirfichas(){
    a={
        type:'repartir',
        message:'distribucion de fichas',
        numUsuario:miConexion,
        username: nombreUsuario
    };
    var mes=JSON.stringify(a);
    websocket.send(mes);

    posicionesArreglo = jugadores-1;
    esconderFichasL();
}

function roboFicha(data){

    if(nombreUsuario==jugadorActual){
        a={
            type:'pilaFichas',
            message: data,
            username: nombreUsuario
        };
        var mes=JSON.stringify(a);
        websocket.send(mes);
    }else{
        Swal.fire({
            icon: 'error',
            title: 'No es tu turno',
            showConfirmButton: false,
            timer: 2000
        });
    }
}

function pintarRobada(fichaR){
    var misfichitas = document.getElementById('misfichasAbajo').innerHTML;
        datos = "";
        datos += '<div class="ui card fichaL misfichas R'+fichaR[0].color+'R'+fichaR[0].numero+'R'+fichaR[0].tipo+'R" value = '+fichaR[0].color+'>';
        if (fichaR[0].tipo=="comodin") {
            datos+='<img id= "imagenComodin" src="img/comodin.png" alt = "comodin">';
        }else{
            datos += '<h2 class="numeroFicha color' + fichaR[0].color + '">' +fichaR[0].numero + '</h2>';        }
        
        datos += '</div>';
        misfichitas += datos;
        $(".audioPieza")[0].play();
    $("#misfichasAbajo").empty();
    $("#misfichasAbajo").append(misfichitas);
    fichasDragg();
    // $( ".ui.card.fichaL.misfichas" ).draggable({ revert: "invalid", snap: "true", snapMode: "inner" });
    actualizarRoboExternos();
}

function actualizarRoboExternos(){
    a={
        type:'RoboJugadorExterno',
        message:'robe una ficha',
        username: nombreUsuario
    };
    var mes=JSON.stringify(a);
    websocket.send(mes);
}

function pintarRoboExterno(indexusuario,cantijugadores){
    if(miConexion==1){
        if(indexusuario==2){
            if(cantijugadores==2){
                pintarRoboExterno2("Arriba");
            }else{
                pintarRoboExterno2("Izquierda");
            }
       }else if(indexusuario==3){
            pintarRoboExterno2("Arriba");
       }else if(indexusuario==4){
            pintarRoboExterno2("Derecha");
       }
    }else if(miConexion==2){
        if(indexusuario==1){
            if(cantijugadores==2 || cantijugadores==3){
                pintarRoboExterno2("Arriba");
            }else{
                pintarRoboExterno2("Derecha");
            }
       }else if(indexusuario==3){
            pintarRoboExterno2("Izquierda");
       }else if(indexusuario==4){
            pintarRoboExterno2("Arriba");
       }
    }else if(miConexion==3){
        if(indexusuario==1){
            if(cantijugadores==3){
                pintarRoboExterno2("Izquierda");
            }else{
                pintarRoboExterno2("Arriba");
            }
       }else if(indexusuario==2){
            if(cantijugadores==3){
                pintarRoboExterno2("Arriba");
            }else{
                pintarRoboExterno2("Derecha");
            }
       }else if(indexusuario==4){
            pintarRoboExterno2("Izquierda");
       }
    }else{
        if(indexusuario==1){
            pintarRoboExterno2("Izquierda");
       }else if(indexusuario==2){
            pintarRoboExterno2("Arriba");
       }else if(indexusuario==3){
            pintarRoboExterno2("Derecha");
       }
    }
}

function pintarRoboExterno2(posicion){
    var fichitas = document.getElementById('fichasexternas'+posicion).innerHTML;
    fichitas += '<div class="ui card fichaL fichaext'+posicion+'">';
    fichitas += '<h2 class="fichaexterna">R</h2>';
    fichitas += '</div>';
    $("#fichasexternas"+posicion).empty();
    if(posicion=="Arriba"){
        $("#fichasexternas"+posicion).prepend(fichitas);
    }else{
        $("#fichasexternas"+posicion).append(fichitas);
    }
    
}

function actualizarFichasExternas(contFichas,jugadorA){
   
    if(jugadores>=2){
        if(document.getElementById("nArriba").innerHTML==jugadorA){
            $('#fichasexternasArriba').empty();
            $('#fichasexternasArriba').prepend(pintarReparticionExt("Arriba",contFichas));
            
        }else{
          
        }
    }
    if(jugadores>=3){
        if(document.getElementById("nIzquierda").innerHTML==jugadorA){
            $('#fichasexternasIzquierda').empty();
            $('#fichasexternasIzquierda').append(pintarReparticionExt("Izquierda",contFichas));
        }else{
            
        }
    }
    if(jugadores>=4){
        if(document.getElementById("nDerecha").innerHTML==jugadorA){
            $('#fichasexternasDerecha').empty();
            $('#fichasexternasDerecha').prepend(pintarReparticionExt("Derecha",contFichas));
        }else{
           
        }
    } 

}

function pintarReparticion(misfichas){
    var fichita="";
    fichita+='<div class="ui container">';
    fichita+='<div id="misfichasAbajo" class="ui centered cards">';
    for (let index = 0; index < misfichas.length; index++) {
        
        datos = "";
        var color = '\''+ misfichas[index].color +'\'';        
        datos += '<div class="ui card fichaL misfichas R'+misfichas[index].color+'R'+misfichas[index].numero+'R'+misfichas[index].tipo+'R" value = '+color+'>';
        if (misfichas[index].tipo=="comodin") {
            datos+='<img id= "imagenComodin" src="img/comodin.png" alt = "comodin">';
        }else{
            datos += '<h2 class="numeroFicha color' + misfichas[index].color + '">' + misfichas[index].numero + '</h2>';
        }
        // datos += '<div id = "prueba" class="ui card fichaL misfichas">';
        datos += '</div>';
        fichita += datos;
    }
    fichita += '</div>';
    fichita += '</div>';

    $("#Abajo").append(fichita);

   fichasDragg();
   sort();

    $("#Abajo").hide();
    $("#Abajo").transition('fly down',2000);
    if(jugadores==2){
        $("#Arriba").prepend(pintarReparticionExt("Arriba",14));
        $("#Arriba").hide();
        $("#Arriba").transition('fly up',2000);
    }else if (jugadores==3){
        $("#Arriba").prepend(pintarReparticionExt("Arriba",14));
        $("#Izquierda").append(pintarReparticionExt("Izquierda",14));
        $("#Arriba").hide();
        $("#Arriba").transition('fly up',2000);
        $("#Izquierda").hide();
        $("#Izquierda").transition('fly left',2000);
    }else{
        $("#Arriba").prepend(pintarReparticionExt("Arriba",14));
        $("#Izquierda").append(pintarReparticionExt("Izquierda",14));
        $("#Derecha").append(pintarReparticionExt("Derecha",14));

        $("#Arriba").hide();
        $("#Arriba").transition('fly up',2000);
        $("#Izquierda").hide();
        $("#Izquierda").transition('fly left',2000);
        $("#Derecha").hide();
        $("#Derecha").transition('fly right',2000);
    }
    $('#robar').transition('fly left',2000);
    $( ".ui.card.fichaL.misfichas" ).draggable("disable");

    tableroalInicio=document.getElementById('body').innerHTML;
    contadorSortsInicio=contadorSorts;
    gestionTurnos(names[playerName]);
}



function fichasDragg(){
    $( ".ui.card.fichaL.misfichas" ).draggable({ revert: "invalid", snap: "true", snapMode: "inner" });
    $( ".ui.card.fichaL.misfichas" ).draggable({
        connectToSortable: ".sortable",
        revert: "invalid"
      });
}


function sort(){
    var sortableElegido;
    var elemento;
    $(".sortable").sortable({
        revert: true,
        start: function(event,ui){
         
            sortableElegido = $(this).prop('class');
            var start_pos = ui.item.index();
           
            var color = ui.item.val();
            ui.item.data('start_pos', start_pos);
            switch (start_pos) {
                case 1:$(this).width(115);break;
                case 2:$(this).width(115);break;
                case 3:$(this).width(154);break;
                case 4:$(this).width(192);break;
                case 5:$(this).width(229);break;
                case 6:$(this).width(268);break;
                case 7:$(this).width(305);break;
                case 8:$(this).width(344);break;
                case 9:$(this).width(382);break;
                case 10:$(this).width(420);break;
                case 11:$(this).width(457);break;
                case 12:$(this).width(495);break;
                case 13:$(this).width(533);break;
               default:
                    break;
            }

        },
        update: function(event,ui){
            var start_pos = ui.item.data('start_pos');
            if(start_pos==0){
                contadorSorts++;
                data = {
                    type : 'contadorSort',
                    cant : contadorSorts
                };
                var dataToSend = JSON.stringify(data);
                websocket.send(dataToSend);               
                datos='<div id = "sortablea'+contadorSorts+'" class="sortable a'+contadorSorts+'">';

                datos+='</div>';
                $("#sortableGrande").append(datos);
                sort();
                }
           
            $(".sortable").sortable();
            switch (start_pos) {
                case 1:$(this).width(115);break;
                case 2:$(this).width(115);break;
                case 3:$(this).width(154);break;
                case 4:$(this).width(192);break;
                case 5:$(this).width(229);break;
                case 6:$(this).width(268);break;
                case 7:$(this).width(305);break;
                case 8:$(this).width(344);break;
                case 9:$(this).width(382);break;
                case 10:$(this).width(420);break;
                case 11:$(this).width(457);break;
                case 12:$(this).width(495);break;
                case 13:$(this).width(533);break;
               default:
                    break;
            }

            var sortableGrande = document.getElementById("sortableGrande").innerHTML;
            data = {
                type:'updateSortable',
                sortableGrande:sortableGrande
            };

            var dataToSend = JSON.stringify(data);
            websocket.send(dataToSend);
        }
      });
}

function pintarReparticionExt(posicion,fich){
    var fichita="";
    // fichita+='<div class="ui container">';
            fichita+='<div id="fichasexternas'+posicion+'" class="ui centered cards">';
    for (let index = 0; index < fich; index++) {
        datos = "";
        datos += '<div class="ui card fichaL fichaext'+posicion+'">';
            datos += '<h2 class="fichaexterna">R</h2>';
        datos += '</div>';
        fichita += datos;
    }
    fichita += ' </div>';
    // fichita += '</div>';
    return fichita
}

        function pintarmensaje(valor, username){
            if(miConexion==1){
                if(valor==1){
                    datos='<p id="minombre" class="nombreAbajo">'+username+'</p>';
                    // datos='<p id="nombre'+valor+'">'+username+'</p>';
                   $("#Abajo").append(datos);

               }else if(valor==2){

                   datos='<p id="nArriba"  class="nombreArriba">'+username+'</p>';
                   player2='<p id="nIzquierda" class="nombreIzquierda">'+username+'</p>';
                   $("#Arriba").append(datos);

               }else if(valor==3){

                   datos='<p id="nArriba" class="nombreArriba">'+username+'</p>';     
                   $("#Arriba").empty();
                   $("#Arriba").append(datos);
                   $('#Izquierda').append(player2);

               }else if(valor==4){

                   datos='<p id="nDerecha" class="nombreDerecha">'+username+'</p>';
                   $("#Derecha").append(datos);
               }
            }else if(miConexion==2){

                if(valor==1){

                    datos='<p id="nArriba" class="nombreArriba">'+username+'</p>';
                    player2='<p id="nDerecha" class="nombreDerecha">'+username+'</p>';
                   $("#Arriba").append(datos);

               }else if(valor==2){

                //    datos='<p id="nombre'+valor+'">'+username+'</p>';
                   datos='<p id="minombre" class="nombreAbajo">'+username+'</p>';
                   $("#Abajo").append(datos);

               }else if(valor==3){

                   datos='<p id="nIzquierda" class="nombreIzquierda">'+username+'</p>';
                   $("#Izquierda").append(datos);
             
               }else if(valor==4){
                   datos='<p id="nArriba" class="nombreArriba">'+username+'</p>';
                   $("#Arriba").empty();
                   $("#Derecha").append(player2);
                   $("#Arriba").append(datos);
               }
            }else if(miConexion==3){

                if(valor==1){

                    datos='<p id="nIzquierda" class="nombreIzquierda">'+username+'</p>';
                    player1='<p id="nArriba" class="nombreArriba">'+username+'</p>';
                   $("#Izquierda").append(datos);

               }else if(valor==2){

                   datos='<p id="nArriba" class="nombreArriba">'+username+'</p>';
                   player2='<p id="nDerecha" class="nombreDerecha">'+username+'</p>';
                   $("#Arriba").append(datos);

               }else if(valor==3){

                //    datos='<p id="nombre'+valor+'">'+username+'</p>';
                   datos='<p id="minombre" class="nombreAbajo">'+username+'</p>';
                   $("#Abajo").append(datos);
                
               }else if(valor==4){

                   datos='<p id="nIzquierda" class="nombreIzquierda">'+username+'</p>';
                   $("#Izquierda").empty();
                   $("#Arriba").empty();
                   $("#Arriba").append(player1);
                   $("#Derecha").append(player2);
                   $("#Izquierda").append(datos);
               }
            }else{

                if(valor==1){

                    datos='<p id="nIzquierda" class="nombreIzquierda">'+username+'</p>';
                   $("#Izquierda").append(datos);

               }else if(valor==2){

                   datos='<p id="nArriba" class="nombreArriba">'+username+'</p>';
                   $("#Arriba").append(datos);

               }else if(valor==3){

                   datos='<p id="nDerecha" class="nombreDerecha">'+username+'</p>';
                   $("#Derecha").append(datos);
               
               }else if(valor==4){

                //    datos='<p id="nombre'+valor+'">'+username+'</p>';
                   datos='<p id="minombre" class="nombreAbajo">'+username+'</p>';
                   $("#Abajo").append(datos);
               }
            }
        }


        function ValidarJugada() {
            var Arreglo = [];
            var verificacionE;
            for (let index = 1; index < contadorSorts; index++) {
                Arreglo = [];
                var contenido = document.getElementById("sortablea"+index).innerHTML;
                var splitFichas = contenido.split('</div>');
               
                if (splitFichas.length==1) {
                    verificacionE="true";
                }
                for (let i = 0; i < splitFichas.length-1; i++) {
                    var splitParametros = splitFichas[i].split('R');

                    data = {
                        color: splitParametros[1],
                        numero: splitParametros[2],
                        tipo: splitParametros[3]
                    }
                    
                    Arreglo[i] = JSON.stringify(data);
                }

                

                if (Arreglo.length>=3) {
                    verificacionE = verificarEscalera(Arreglo);
                }else if( Arreglo.length > 0 && Arreglo.length < 3 ){
                    verificacionE="false";
                }

              if (verificacionE=="false") {
                  break;
              }
   
            }

         
            return verificacionE;
        }

        function verificarEscalera(Arreglo) {
            var arreglito = JSON.parse(Arreglo[0]);
            var comparacion = arreglito.color;
            var esEscalera = "si";
            var numeros=0;
            var numeroposterior=0;
            numeros = arreglito.numero;
            
            if(numeros==0){
                var arreglitos = JSON.parse(Arreglo[1]);
                numeros=(arreglitos.numero)-1;
                comparacion=arreglitos.color;
            }
            numeros=numeros-1;
            var consecucion = "si";
        
            if(Arreglo.length>=4){
                for (let j = 0; j < Arreglo.length; j++) {
                    var arreglito2 = JSON.parse(Arreglo[j]);
                    if (arreglito2.color!=comparacion && arreglito2.tipo=="normal") {
                        esEscalera = "no";
                    }
                    numeroposterior = parseInt(numeros)+1;
                    // numeroposterior=numeroposterior+1;
                    if (arreglito2.numero==numeroposterior || arreglito2.tipo=="comodin") {
                        if(arreglito2.tipo=="comodin"){
                         numeros=numeroposterior;
                        }else{
                            numeros = arreglito2.numero;
                        }
                    }else{
                        consecucion = "no";
                        break;
                    }
                    
                }
            }else {
                esEscalera="no";
            }
            if (esEscalera == "no"||consecucion=="no") {
                
                var verificarS = verificarSerie(Arreglo);
                if (verificarS=="true") {
                    return "true";
                }else{
                    return "false";
                }
            }else if(esEscalera=="si"&&consecucion=="si"){
                return "true";
            }
        }



        function verificarSerie(Arreglo) {
            var arreglito = JSON.parse(Arreglo[0]);
            var esSerie = "si";
            var consecucionNumero = "si";
            comparacion = arreglito.numero;
            if(comparacion==0){
                var arreglitos = JSON.parse(Arreglo[1]);
                comparacion=arreglitos.numero;
            }
                   
                    for (let j = 0; j < Arreglo.length; j++) {
                        var arreglito2 = JSON.parse(Arreglo[j]);
                        var comparacion2 = arreglito2.color;
                        

                        for (let k = j+1; k < Arreglo.length; k++) {
                            var arreglito3 = JSON.parse(Arreglo[k]);
                         
                            if (arreglito3.color==comparacion2 && arreglito3.tipo=="normal") {
                                esSerie = "no";
                                break;
                            }
                            
                        }

                        if (arreglito2.numero!=comparacion && arreglito2.tipo=="normal" ) {
                            consecucionNumero = "no";
                            break;
                        }
                    }

                    if (esSerie =="no"||consecucionNumero=="no") {
                 
                        return "false";
                    }else if(esSerie =="si"&&consecucionNumero=="si"){
                      
                        return "true";
                    }
        }
 
////////////////////////////////////////////////

        function enviarMensaje(texto,valor) {
            
            if(valor==0){
                a={
                    type:'message',
                    message:texto,
                    username:nombreUsuario
                };
                var mes=JSON.stringify(a);
                websocket.send(mes);
                pintarmensaje(texto, 1,nombreUsuario);
            }else{
                websocket.send(texto);
            }
           
        }

//Con esta funcion Ping, se mantiene la conexion con el servidor enviando mensajes periodicos
        
        function ping(){
        
        mensaje={
            type:"ping",
            message:"heartbeating"
        };
        var prom= wait(28000);
         var showDone=()=>enviarMensaje(JSON.stringify(mensaje),1);
         prom.then(showDone)
        }