package rummy;

public class Ficha {
	private String color;
	private int numero;
	private String tipo;
	public Ficha(String color, int numero, String tipo) {
		this.color = color;
		this.numero = numero;
		this.tipo = tipo;
	}
	public Ficha() {
		this.color = "all";
		this.numero = 0;
		this.tipo = "comodin";
	}
	
	@Override
	public String toString() {
		return "Ficha [color=" + color + ", numero=" + numero + ", tipo=" + tipo + "]";
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
