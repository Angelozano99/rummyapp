package rummy;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

public class GestionJuego {
	private static List<Ficha> fichas; 
    private List fichasrepartidastotal=new ArrayList<>();
    private List fichasrepartidasindividual=new ArrayList<>();
    private List jugadoresRepartidos=new ArrayList<>();
    
    
    public GestionJuego() {
		
	}

	public void GestionInicial() {
	    fichas = new ArrayList<Ficha>();
		String[] colores= {"rojo","negro","azul","amarillo"};
		for (int i = 0; i<4; i++) {
			String color = colores[i];
			for (int j = 1; j <= 13; j++) {
				Ficha fichita = new Ficha(color,j,"normal");
				fichas.add(fichita);
				fichas.add(fichita);
			}
		}
		Ficha comodin = new Ficha();
		fichas.add(comodin);
		Ficha comodin2 = new Ficha();
		fichas.add(comodin2);
    }
    
	public List fichaRobada(String usuario) {
		ArrayList arraysito = new ArrayList<>();
		ArrayList arraysito2 = new ArrayList<>();
		
	 	if(!fichas.isEmpty()) { 
			int posicion = (int) (Math.random() * fichas.size() + 1);
			posicion -= 1;
			JSONObject obj = new JSONObject();
			obj.put("color", fichas.get(posicion).getColor());
			obj.put("numero", fichas.get(posicion).getNumero());
			obj.put("tipo", fichas.get(posicion).getTipo());
	
			arraysito.add(obj.toString());
			
			for (int i = 0; i < jugadoresRepartidos.size(); i++) {
				
				if (usuario.equals(jugadoresRepartidos.get(i))) {
					arraysito2 = (ArrayList) fichasrepartidasindividual.remove(i);
					arraysito2.add(fichas.get(posicion));
					fichasrepartidasindividual.add(i, arraysito2);
				}
			}
			fichasrepartidastotal.add(fichas.remove(posicion));
			return arraysito;
	 	}else {
	 		return arraysito;
	 	}
    }
	
	public List repartirFichas(String usuario) { 
    	ArrayList arraysito= new ArrayList<>();
    	ArrayList arraysito2= new ArrayList<>();
    	for (int i = 0; i < 14; i++) {
    		int posicion=(int) (Math.random()*fichas.size()+1);
    		posicion-=1;
    			JSONObject obj = new JSONObject();
        		obj.put("color", fichas.get(posicion).getColor());
        		obj.put("numero", fichas.get(posicion).getNumero());
        		obj.put("tipo", fichas.get(posicion).getTipo());   		
//        		System.out.println(obj.toString());
        		arraysito2.add(fichas.get(posicion));
        		arraysito.add(obj.toString());
        		fichasrepartidastotal.add(fichas.remove(posicion));
		}
    	jugadoresRepartidos.add(usuario);
    	fichasrepartidasindividual.add(arraysito2);
    	return arraysito;
    }
	
	public List inicioPartida(int cont) {
		   
    	ArrayList arraysito= new ArrayList<>();
    	ArrayList fichasG=new ArrayList<>();
    	ArrayList mayor=new ArrayList<>();
    	int contmayor;
    
    	for (int i = 0; i < cont; i++) {
    		contmayor=0;
    		int posicion=(int) (Math.random()*fichas.size()+1);
    		posicion-=1;
    		if( fichas.get(posicion).getTipo()=="normal") {
    			JSONObject obj = new JSONObject();
        		obj.put("color", fichas.get(posicion).getColor());
        		obj.put("numero", fichas.get(posicion).getNumero());
        		obj.put("tipo", fichas.get(posicion).getTipo());
        	    mayor.add(fichas.get(posicion).getNumero());

        	   for (int j = 0; j < mayor.size(); j++) {
   				if((int)mayor.get(j)==fichas.get(posicion).getNumero()) {
   					contmayor++;
   				}
   			}
        	   if(contmayor==1) {
        			arraysito.add(obj.toString());
            		fichasG.add(fichas.remove(posicion));
        	   }else {
        		   i--;
        	   }	
    		}else {
    			i--;
    		}
		}
    	
    	for (int i = fichasG.size()-1; i >=0 ; i--) {
    		fichas.add((Ficha) fichasG.remove(i));
		}
    	return arraysito;
    }
}
