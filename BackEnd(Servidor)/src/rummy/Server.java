package rummy;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONException;
import org.json.JSONObject;


public class Server extends WebSocketServer{
	//private static Map<Integer,Set<WebSocket>> Rooms = new HashMap<>();
	
	private static List<WebSocket> clients=new ArrayList<WebSocket>();
	private static List<String> users=new ArrayList<String>();
	private static List<Ficha> turnos; 
	private static GestionJuego gestion = new GestionJuego();
    public Server() {
        super(new InetSocketAddress(30001));
    }
    int cont=0;
    
    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        System.out.println("New client connected: " + conn.getRemoteSocketAddress() + " hash " + conn.getRemoteSocketAddress().hashCode());
        
        if(clients.size()>=4) {
        	JSONObject objeto = new JSONObject();
            objeto.put("type", "Overflow");
            objeto.put("message", "Sala llena");
        	conn.send(objeto.toString());
        }else {
        	clients.add(conn);
        	JSONObject objeto = new JSONObject();
            objeto.put("type", "NoOverflow");
            objeto.put("message", "Sala libre");
        	conn.send(objeto.toString());
        }
        //Rooms.put(cont, (Set<WebSocket>) conn);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        JSONObject obj = new JSONObject(message);
        Set<WebSocket> s;
        try {
            String msgtype = obj.getString("type");
            if(msgtype.equals("ping")) {
            	
            	conn.send("{\"type\":\"message\",\"message\":\"pong\"}");
            	
            }else if(msgtype.equals("message")) {
            	sendToAll(conn,message);
            }else if(msgtype.equals("username")) {
            	users.add(obj.getString("username"));
            	cont++;
                JSONObject objeto = new JSONObject();
                objeto.put("type", "Conexionexterno");
                objeto.put("message", "Se ha conectado un externo");
                objeto.put("username", obj.getString("username"));
                objeto.put("conectados", cont);
                sendToAll(conn, objeto.toString());
                objeto = new JSONObject();
                objeto.put("type", "Conexionlocal");
                objeto.put("Users", users.toString());
                objeto.put("conectados", cont);
                conn.send(objeto.toString());
            }
            if(msgtype.equals("inicioPartida")) {

            	turnos=gestion.inicioPartida(cont);
            	JSONObject objeto = new JSONObject();
                objeto.put("type", "sorteoTurnos");
                objeto.put("message", turnos.toString());
            	sendToAll(conn,objeto.toString());
            	conn.send(objeto.toString());
            	
            }
            if(msgtype.equals("repartir")) {
            	List distribucion = gestion.repartirFichas(obj.getString("username"));
            	JSONObject objeto = new JSONObject();
                objeto.put("type", "mireparticion");
                objeto.put("message", distribucion.toString());
                conn.send(objeto.toString());
            }
            
            if(msgtype.equals("jugadorActual")) {
            	  
            	sendToAll2(conn, message);

            }
            if(msgtype.equals("pilaFichas")) {
	            List robo = gestion.fichaRobada(obj.getString("username"));
	            JSONObject objeto = new JSONObject();
	            if(!robo.isEmpty()) {
	            	objeto.put("type", "pilaFichasR");
		            objeto.put("message",robo.toString());
		            objeto.put("tipoJugada", obj.getString("message"));
		            conn.send(objeto.toString());
	            }else {
	            	objeto.put("type", "empate");
		            objeto.put("message","el juego esta empatado");
	            	sendToAll2(conn,objeto.toString());
	            }
            }
            if(msgtype.equals("RoboJugadorExterno")) {
          
            	sendToAll(conn, message);

            }
            if(msgtype.equals("updateSortable")) {
            	sendToAll(conn, message);
            }
            if(msgtype.equals("jugadaErrada")) {
            	sendToAll(conn, message);
            }
            if(msgtype.equals("contadorSort")) {
            	sendToAll(conn, message);
            }
            if(msgtype.equals("Verifyusername")) {
            	String usuarioarreglo="";
            	String usuarionuevo=obj.getString("username");
            	int cont=0;
            	JSONObject object = new JSONObject();
            	object.put("type", "Validacionusername");
            	object.put("username", usuarionuevo);
            	Iterator it = users.iterator();
            	while(it.hasNext()) {
            		usuarioarreglo = (String) it.next();
            		if(usuarioarreglo.equalsIgnoreCase(usuarionuevo)) {
            			cont++;
            			break;
            		}
            	}
            	if(cont==0) {
        			object.put("message", "nombre valido");
            		conn.send(object.toString());
            	}else {
        			object.put("message", "nombre no valido");
            		conn.send(object.toString());
            	}
            }
            
//            switch (msgtype) {
//                case "GETROOM":
//                    myroom = generateRoomNumber();
//                    s = new HashSet<>();
//                    s.add(conn);
//                    Rooms.put(myroom, s);
//                    System.out.println("Generated new room: " + myroom);
//                    conn.send("{\"type\":\"GETROOM\",\"value\":" + myroom + "}");
//                    break;
//                case "ENTERROOM":
//                    myroom = obj.getInt("value");
//                    System.out.println("New client entered room " + myroom);
//                    s = Rooms.get(myroom);
//                    s.add(conn);
//                    Rooms.put(myroom, s);
//                    break;
//                default:
//                    sendToAll(conn, message);
//                    break;
            
//            }
        } catch (JSONException e) {
            sendToAll(conn, message);
        }
    	//conn.send("listo! recibido!");
    	
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        System.out.println("Client disconnected: " + reason);
    }

    @Override
    public void onError(WebSocket conn, Exception exc) {
        System.out.println("Error happened: " + exc);
    }

    private int generateRoomNumber() {
        return new Random(System.currentTimeMillis()).nextInt();
    }

    private void sendToAll(WebSocket conn, String message) {
       
//    	Iterator it = ((Set<WebSocket>) Rooms).iterator();
//        while (it.hasNext()) {
//            WebSocket c = (WebSocket)it.next();
//            if (c != conn) 
//            	if(c==)
//            	c.send(message);
//            
//        }
        
    	for(int i =0;i<clients.size();i++) {
    		WebSocket c = (WebSocket)clients.get(i);
            if (c != conn) c.send(message);
    	}
    }
    private void sendToAll2(WebSocket conn, String message) {
    
    	for(int i =0;i<clients.size();i++) {
    		WebSocket c = (WebSocket)clients.get(i);
            c.send(message);
    	}
    }
    
    public static void main(String[] args) {
    	gestion.GestionInicial();
        Server server = new Server();
        server.start();
    }
    

}
